<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( !function_exists( 'funiter_child_locale_css' ) ):
  function funiter_child_locale_css( $uri ){
    if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
      $uri = get_template_directory_uri() . '/rtl.css';
    return $uri;
  }
endif;
add_filter( 'locale_stylesheet_uri', 'funiter_child_locale_css' );

function bfriend_load_scripts() {
  $js_full = get_stylesheet_directory_uri() . '/assets/js/';

  wp_dequeue_script( 'funiter-script' );
  wp_deregister_script( 'funiter-script' );

  if (!is_admin()){
		wp_enqueue_script('libs', 				$js_full . 'libs.js', ['jquery']);
    wp_enqueue_script('child-funiter-script', 				$js_full . 'main.js', ['jquery']);

  }
}
add_action( 'wp_enqueue_scripts', 'bfriend_load_scripts' , 100);

/* Tamanho das Imagens do Woocommerce */
/* ----------------------------------------- */
	add_filter( 'woocommerce_get_image_size_single', function( $size ) {
		return [
      'width' => 2000, 'height' => 2000, 'crop' => 0
    ];
	});    
/* ----------------------------------------- Tamanho das Imagens do Woocommerce */