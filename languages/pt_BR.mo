��    O      �  k         �  
   �     �     �     �     �     �  	   �               	                    "     *     8     @     D  
   S     ^     o     w     {     �  	   �     �     �     �     �     �     �     �     �            	   2     <     A     Q     ^     f     v     ~     �     �  
   �     �     �     �     �     �     �  $   �      	  
   &	     1	     7	     E	     N	     c	     k	  :   	     �	  q   �	  7   >
     v
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
  $   �
       "     s  0  	   �     �     �     �     �     �     �     �     �     �  	   �  
     
     
        $  
   8     C     G     U     f  	   �     �     �     �  	   �     �     �     �     �     �     �     
          -     D     X     g     l     ~     �     �  
   �     �     �     �  
   �     �     �     �               *  )   =     g     n     z     �     �     �  	   �     �  =   �       �   /  7   �  $   �                    "     5     I     U     b     f  -   s  
   �     �         ?       N          6                I   &       M   2               '          %       H   3         <         .   "         *         	       !      $   7   5   E   -   
      O       (   >          G       K             L           4   9                0          F       J   D       ,          @       8          :           B      )       A      =   #      1      +   /   ;       C         Read more  Sticky  already claimed % %s (Invalid) %s (Pending) (Unit px) ... 0 1 1 Star 2 Stars 3 Stars 4 Stars 404 Not Found 5 Stars ASC Active Section Add a menu Add name member. Address All All Categories All author posts Anti-spam Archives Author Availability: Available:  Avatar Member Back to hompage Back to login Banner Image Best Selling Products Bestseller Products Big Title Blog Blog List style Blog widget. Brand:  Browse Wishlist Browse: Button Button text By  CATEGORIES CSS Classes (optional) Cancel Categories: Categories:  Category List style Choose category Choose the target to filter products Clear Clear All  Close Comment count Comments Comments are closed. Content Continue reading %s Create WordPress loop, to populate content from your site. Create an account Create an account to expedite future checkouts, track order history & receive emails, discounts, & special offers Creates 2 different text versions on mobile and desktop Cross Sell Products Dark Date Days Deal ends in : Default sorting Description Descriptions Yes Your cart Your comment is awaiting moderation. address. breadcrumbs aria labelBreadcrumbs Project-Id-Version: Funiter
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-25 03:03+0000
PO-Revision-Date: 2020-04-07 20:52-0300
Language-Team: 
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Loco-Version: 2.2.2; wp-5.2
Last-Translator: 
Language: pt_BR
 Leia mais  Fixo já reivindicado % %s (Inválido) %s (Pendente) (Unidade px) ... 0 1 1 Estrela 2 Estrelas 3 Estrelas 4 Estrelas 404 não encontrado 5 Estrelas ASC Seção ativa Adicione um menu Adicione o nome do membro. Endereço Todos Todas Categorias Todos os posts do autor Anti-spam Arquivos Autor Disponibilidade: Disponível:  Avatar Voltar para Home Voltar para Login Imagem do Banner Produtos mais vendidos Bestseller Products Título Grande Blog Blog estilo lista Blog widget. Marca:  Ver lista de desejos Pesquisar: Botão Texto do botão Por  CATEGORIAS Classes CSS (opcional) Cancelar Categorias: Categorias:  Categoria estilo lista Escolher categoria Escolha o destino para filtro de produtos Limpar Limpar tudo Fechar Contador de comentários Comentários Comentários estão fechados. Conteúdo Continue Lendo %s Criar loop WordPress, para preencher o conteúdo de seu site. Criar uma conta Crie uma conta para agilizar as compras futuras, acompanhar o histórico de pedidos e receber e-mails, descontos e ofertas especiais Cria 2 versões de texto diferentes no mobile e desktop Colunas de produtos de venda cruzada Escuro Data Dias Oferta termina em: Ordenação Padrão Descrição Descrições Sim Seu carrinho Seu comentário está aguardando moderação. endereço. Breadcrumbs 